#!/usr/bin/env python3
# coding: utf-8


from rpi_ws281x import Color
from datetime import datetime

from time import sleep

# noinspection PyUnresolvedReferences
from rpi_ws281x import ws, Color, Adafruit_NeoPixel

LED_COUNT = 60  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 100  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP = ws.SK6812_STRIP  # Strip type and colour ordering


class Strip:
    class Digit:
        registers = [5, 0, 6, 1, 7, 2, 8, 3, 9, 4]

        def __init__(self, value=0, color=Color(0, 0, 0, )):
            self.value = self.registers[value]
            self.color = color

        def set(self, value, color):
            self.value = self.registers[value]
            self.color = color

    def __init__(self):
        self.digits = [self.Digit(), self.Digit(), self.Digit(), self.Digit(), self.Digit(), self.Digit()]
        self.strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS,
                                       LED_CHANNEL, LED_STRIP)
        self.strip.begin()

    def clear(self, show=True):
        for i in range(LED_COUNT):
            self.strip.setPixelColor(i, Color(0, 0, 0))
        if show:
            self.show()

    def set_color(self, color):
        self.clear(show=False)
        for decade, digit in enumerate(self.digits):
            self.strip.setPixelColor(decade * 10 + digit.value, color[decade])

    def show(self):
        self.strip.show()

    def number(self, num):
        self.clear(show=False)
        for i in range(len(self.digits) - 1, -1, -1):
            self.digits[i].set(int(num % 10), None)
            num /= 10

    @property
    def brightness(self):
        return self.strip.getBrightness()

    @brightness.setter
    def brightness(self, val):
        val = int(val)
        if val < 0 or val > 255:
            raise AttributeError
        self.strip.setBrightness(val)
        self.strip.show()


class LEDNixieController:
    def __init__(self, pattern, content):
        self.strip = Strip()
        self.pattern = pattern
        self.content_func = content
        self.wait_between_steps_ms = 2

    def run(self):
        try:
            while True:
                self.strip.number(self.content_func())
                self.strip.set_color(self.pattern.step())
                self.strip.show()
                sleep(self.wait_between_steps_ms / 1000.0)
        except:
            raise
        finally:
            self.strip.clear(show=True)


time = lambda: int(datetime.now().strftime("%H%M%S"))
date = lambda: int(datetime.now().strftime("%d%m%y"))

start = datetime.now()
dummy = datetime(2000, 1, 1)

class Pattern:
    current_color = Color(255, 255, 255)

    def step(self):
        return [self.current_color] * 6

colors = [
    [148, 0, 211],  # purple
    [75, 0, 130],  # indigo
    [0, 0, 255],  # blue
    [0, 255, 0],  # green
    [255, 255, 0],  # yellow
    [255, 127, 0],  # orange
    [255, 0, 0],  # red
]

class DigitPattern:
    current_color = Color(255,255,255)
    def __init__(self, steps=100, current_index=0):
        self.current_index = current_index
        self.current_step = 0
        self.steps = steps
        self.current_r = colors[current_index][0]
        self.current_g = colors[current_index][1]
        self.current_b = colors[current_index][2]

    def step(self):
        next_index = (self.current_index + 1) % len(colors)
        colorFrom = colors[self.current_index]
        colorTo = colors[next_index]

        step_R = (colorTo[0] - colorFrom[0]) / self.steps
        step_G = (colorTo[1] - colorFrom[1]) / self.steps
        step_B = (colorTo[2] - colorFrom[2]) / self.steps

        self.current_color = Color(*map(int, (self.current_r, self.current_g, self.current_b)))

        self.current_r += step_R
        self.current_g += step_G
        self.current_b += step_B

        if self.current_step == self.steps - 1:
            self.current_index = next_index
            self.current_step = 0
            self.current_r = colors[self.current_index][0]
            self.current_g = colors[self.current_index][1]
            self.current_b = colors[self.current_index][2]
        else:
            self.current_step += 1
        
        return self.current_color
    
class TestPattern(Pattern):
    def __init__(self, steps=100):
        self.digits = []
        for i in range(6):
            self.digits.append(DigitPattern(steps=steps, current_index=i))
    
    def step(self):
        res = [i.step() for i in self.digits]
        return res

pat = TestPattern(steps=100)

def timer_cs():
    delta = datetime.now() - start
    return int((dummy + delta).strftime("%M%S%f")[:6])

content = time

c = LEDNixieController(pat, content)
c.strip.brightness = 255
c.wait_between_steps_ms = 2
try:
    c.run()
finally:
    c.strip.clear(show=True)

